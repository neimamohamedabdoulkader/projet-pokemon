# Nom du Projet : Prediction de type Pokémon
Projet Pokemon
Description
Le but de ce projet est de construire un modèle de classification d'images de Pokémon en utilisant des techniques de deep learning. Nous avons choisi de travailler avec 4 types de Pokémon : Fairy, Fire, Ghost et Normal. Nous avons collecté des données depuis internet et Kaggle, puis nous avons nettoyé nos données et évité les doublons d'images pour équilibrer les classes. Cependant, nous avons rencontré des difficultés pour le type Ghost car nous n'avions que 74 images, tandis que pour le type Normal nous en avions 1734. Un déséquilibre de ce type peut poser des problèmes de surapprentissage, ce qui nous a poussés à utiliser la méthode SMOTE de suréchantillonnage.

Nous avons également utilisé différentes techniques pour éviter le surapprentissage, telles que l'ajout de couches de dropout et l'utilisation de la régularisation L2 avec différentes valeurs pour trouver le meilleur modèle. Nous continuons à travailler pour améliorer les performances de notre modèle en variant les paramètres et en modifiant le nombre d'époques pour obtenir les meilleurs résultats.

Conclusion
Nous avons pu construire un modèle de classification d'images de Pokémon qui fonctionne bien pour les types Fairy, Fire, Ghost et Normal. Nous avons utilisé différentes techniques pour éviter le surapprentissage et avons surmonté le déséquilibre des classes en utilisant la méthode SMOTE. Nous continuons à travailler sur ce projet pour améliorer les performances de notre modèle et pour ajouter plus de types de Pokemon à notre base de données.
# Membre de groupes 
- Abayazid Mohamed Youssouf
- Mohamed Abdoulkader Neima

